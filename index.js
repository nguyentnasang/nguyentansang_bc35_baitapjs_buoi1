console.log('Bài 1 Tính tiền lương nhân viên')
    /*
    *đầu vào///////////////////////////////////
    -lương 1 ngày: 100.000
    -số ngày làm do người dùng nhập: 30
    *các bước xử lý////////////////////////////////
    -b1: tạo 3 biến. 
        biến wage là lương 1 ngày, 
        biến days là số ngày làm (30ngay), 
        biến total là tiền lương nhân viên được nhận sau 30 ngày
    -b2: gán giá trị cho wage vs days
    -b3:sử dụng công thức tính tiền lương cho biến total
    wage * days
    -b5: in kết quả qua console.log
    *đầu ra/////////////////////////////////////////////
    lương nhân viên: 3.000.000
    */
var wage = 100000;
var days = 30;
var total = wage * days;
console.log("tổng tiền lương trong 30 ngày là: ", total)
    // ...................................................................................
console.log("Bài 2. Tính giá trị trung bình")
    /**
     *đầu vào////////////////////////////////////////// 
     -Người dùng nhập 5 số thực lần lượt là: 1, 2, 3, 4, 5;
    *các bước sử lý////////////////////////////////////////
     -b1 tạo 6 biến 
        biến number1,  number2, number3, number4, number5 là các số thực 1 chữ số
        biến average_value là giá trị trung bình
     -b2:gán giá trị mà người dùng nhập cho các number
     -b3: sử dụng công thức tính trung bình cộng cho biến average_value
     -b4: in kết quả ra console.log
     * đầu ra//////////////////////////////////////////////
    -giá trị trung bình:(1+2+3+4+5)/5=3
      
     */

var number1 = 1;
var number2 = 2;
var number3 = 3;
var number4 = 4;
var number5 = 5;
var average_value = (number1 + number2 + number3 + number4 + number5) / 5
console.log("giá trị trung bình là: ", average_value)
    // ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
console.log('bài 3:Quy đỏi tiền')
    /**
     * đầu vào/////////////////////////////////////////////////// 
     * -giá usd hiện nay 23.500vnd
     * -người dùng nhập 3usd
     * các bước xử lý:////////////////////////////////////////////
     * b1: tạo 3 biến
     *      biến dollar là giá usd hiện nay
     *      biến dong là giá trị người dùng nhập vào để chuyển đỏi sang usd
     *      biến change biến chứa công thức quy đỏi
     * b2:gán giá trị người dùng nhập vào dollar
     * b3: sử dụng công thức quy đỏi tiền cho biến change
     * b4: in kết quả ra console.log
     * 
     * đầu ra///////////////////////////////////////////////////
     * -quy đỏi 3usd sang tiền việt: 
     * 23.500*3=70.500vnd
     */
var dollar = 3;
var dong = 23500;
var change = dollar * dong;
console.log("số tiền sau khi quy đỏi là: ", change)
    // ..............................................................................
console.log("bài 4: tính diện tích, chu vi hình chữ nhật");
/**
 * đầu vào //////////////////////////////////
 * -người dùng nhập chiều dày, rộng của hcn
 * -chiều dày:20
 * -chiều rộng:10
 * các bước xử lý///////////////////////////////////
 * -b1 tạo 4 biến:
 *      biến longs chứa giá trị của chiều dày
 *      biến width chứ giá trị của chiều rộng
 *      biến area chứa công thức tính diện tích HCN
 *      biến perimeter chứa công thức tính chu vi HCN
 * -b2 gán giá trị cho 2 biến longs, width
 * -b3 lập công thức cho 2 biến area và perimeter
 * -b4: gán kết quả ra biến console.log
 * đầu ra////////////////////////////////////////////
 * -diện tích: 20*10=200
 * -chu vi: (20+10)*2=60
 */
var longs = 20;
var width = 10;
var area = longs * width;
var perimeter = (longs + width) * 2;
console.log('diện tích hình chữ nhạt là: ', area)
console.log('chu vi hình chữ nhật là: ', perimeter)
    // ..............................................................................
console.log("bài 5. tính tổng 2 ký số")
    /**
     * đầu vào///////////////////////////////////////////////////////// 
     * -nhập 1 số có 2 chữ số: 82
     * các bước xử lý/////////////////////////////////////////////////
     * -b1 tạo 4 biến
     *      biến n chứa số có 2 chữ số: 82
     *      biến tenth chứa số ở phần chục
     *      biến units chứa số ở phần đơn vị
     *      biến sum chứa công thức tính tổng 2 biến số
     * -b2:gán giá trị cho biến n
     * -b3: lập công thức để lấy giá trị hàng chục hàng đơn vị cho biến tenth, units
     * -b4: lập công thức  tính tổng 2 biến số cho biến sum
     * -b5: in công thức ra console
     * đầu ra///////////////////////////////////////////////////////////
     * -tổng 2 ký số: 8 + 2 = 10
     */

var n = 82;
var tenth = Math.floor(n / 10);
var units = Math.floor(n % 10);
var sum = tenth + units
console.log('tổng của 2 ký số là: ', sum)